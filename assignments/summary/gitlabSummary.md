**GITLAB**

![](Images/1_j9hbjszo0zXS32yhvSkdAQ.jpeg)

GitLab is a single application for the entire DevOps lifecycle that allows teams to work together better and bring more value to customers, faster. 
 
GitLab Interface looks like:

 ![](Images/GitLab_running_11.0__2018-07_.png)