**GIT**

 * Git is a type of version control system (VCS) that makes it easier to track changes to files. For example, when you edit a file, git can help you determine exactly what changed, who changed it, and why.
 * It helps in increasing the coordination among people working on the same code
 * This is the basic understanding of Git
 ![Basic understanding of Git](Images/data-model-4.png)
 * The basic Git command list
 ![Basic Git command list](Images/git-basic-command.png)